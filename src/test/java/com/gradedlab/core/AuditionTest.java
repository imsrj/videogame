package com.gradedlab.core;

import static org.junit.Assert.*;


import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class AuditionTest {
	
	 Audition audition;
		@Before 
		public void setup() {
			
			audition = new Audition();
			audition.addPerformer(1);
			audition.addPerformer(2);
			audition.addPerformer(3);
			audition.addPerformer(4);
			audition.addDancer(5, "tap");
			audition.addDancer(6, "salsa");
			audition.addVocalist(7, 'G');
			//logger.info("This is Test Output:" + audition.startAudition());
			
		}
		
		@Test
		public void testStartAuditionWithInitialized7PerformersPositive() {
			 assertEquals(1,audition.startAudition());
			 
		}
		
		
		@Test
		public void testAddDancerPositiveId() {
			 assertEquals(1,audition.addDancer(8, "salsa"));
		}
		
		@Test
		public void testAddPerformerPositiveId() {
			 assertEquals(1,audition.addDancer(9, "hip-hop"));
		}
		
		@Test
		public void testAddVocalistPositiveId() {
			 assertEquals(1,audition.addVocalist(10, 'A'));
		}
		
		@Test
		public void testAddVocalistWithVolumePositiveId() {
			 assertEquals(1,audition.addVocalistWithVolume(11, 'A', 4));
		}
		
		 @Rule
		   public ExpectedException negativeIdException = ExpectedException.none();
		 
		 @Test
			public void checkIfAnyPerformerAlreadyExistsWithProvidedId() {
				negativeIdException.expect(IllegalArgumentException.class);
				negativeIdException.expectMessage("Perfomer already exists.");
				audition.addDancer(1, "balley");
			}
		
}
