package com.gradedlab.core;

class Dancer extends Performer{
    private final String style;
	Dancer(int id, String style) {
		super(id);
		this.style = style;
		// TODO Auto-generated constructor stub
	}
	
       int perform() {
    	System.out.println(style+ "-" + this.id+ "-" + "dancer");
		return 1;
	}
    
}
